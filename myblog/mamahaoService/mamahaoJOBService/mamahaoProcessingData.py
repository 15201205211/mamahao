class ModelData(object):
    def dataBaseHandle(self, data):
        sqlContent = eval(data["sqlContent"])
        sqllist = {
            "headerData": eval(data['headerData']),
            "dataType": data["dataType"],
            "request_id": data["case_request_id"],
            "caseid": data["otherCaseid"],
            "caseidConclusionValue": data["otherCaseidConclusionValue"],
            "case_name": data["case_name"],
            "domains": eval(data["request_data_content"]),
            "request_other_type": eval(data["request_other_type"]),
            "request_relation_content": eval(data["request_relation_content"]),
            "headers": data["request_header_param"],
            "request_result": eval(data["request_result"]),
            "sqlTrueFlasetype": sqlContent["sqlTrueFlasetype"],
            "sqlChoice": sqlContent["sqlChoice"],
            "SQLinput": sqlContent["SQLinput"],
            "SQLMomentum": sqlContent["SQLMomentum"],
            "sqlClusionValue": sqlContent["sqlClusionValue"],
            "sqlTrueFlasetypeLast": sqlContent["sqlTrueFlasetypeLast"],
            "sqlChoiceLast": sqlContent["sqlChoiceLast"],
            "SQLinputLast": sqlContent["SQLinputLast"],
            "SQLMomentumLast": sqlContent["SQLMomentumLast"],
            'sqlClusionValueLast': sqlContent["sqlClusionValueLast"],

        }
        return dict(sqllist)
