"""EasyTest URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from myblog.mamahaoService.mamahaoUser import  mamahaoServeiceUserlogin
from  myblog.mamahaoService.mamahaoProject import mamahaoProject,mamahaoGroup
from myblog.mamahaoService.mamahaoReport import mamahaoReportAction
from myblog.mamahaoService.mamahaoSystem import mamahaoTestEmail, mamahaotestPersonnel
from myblog.mamahaoService.mamahaoInterfaceManagementList import mamahaoIntereface, mamahaoTestCaseAction, \
    mamahaoPubInter
from  myblog.mamahaoService.mamahaoJOBService import upload
from myblog.mamahaoService.mamahaoTestPlan import mamahaoTestPlanAction
from django.conf.urls import url

urlpatterns = [
    url(r'login$', mamahaoServeiceUserlogin.AuthView.as_view(), ),

    url(r'addProject', mamahaoProject.addProject, ),#Project#项目接口
    url(r'getProject', mamahaoProject.getProject, ),

    url(r'addGroup', mamahaoGroup.addGroup, ),#Group项目分组接口
    url(r'getGroup', mamahaoGroup.getGroup, ),
    url(r'updateGroup', mamahaoGroup.updateGroup, ),
    url(r'deleteGroup', mamahaoGroup.deleteGroup, ),


    url(r'addInterface', mamahaoIntereface.addInterface, ),#Intereface接口管理
    url(r'getInterface', mamahaoIntereface.getInterface, ),
    url(r'updateInterface', mamahaoIntereface.updateInterface, ),
    url(r'deleteInterface', mamahaoIntereface.deleteInterface, ),
    url(r'getRequest', mamahaoIntereface.getRequest, ),

    url(r'addCase', mamahaoTestCaseAction.addCase, ),  # 添加测试用例
    url(r'getCase', mamahaoTestCaseAction.getCase, ),
    url(r'getOneCase', mamahaoTestCaseAction.getOneCase, ),
    url(r'deleteCase', mamahaoTestCaseAction.deleteCase, ),
    url(r'getAllCass', mamahaoTestCaseAction.getAllCass, ),
    url(r'copyCase', mamahaoTestCaseAction.copyCase, ),

    url(r'checkPoint', mamahaoTestCaseAction.checkPoint, ),  # 检查点
    url(r'checkCase', mamahaoTestCaseAction.checkCase, ),  # 检查用例
    url(r'executionCase', mamahaoTestCaseAction.executionCase, ),

    url(r'getTestDB', mamahaoTestCaseAction.getTestDB, ),  # 获取测试库
    url(r'getTestSQL', mamahaoTestCaseAction.getTestSQL, ),  # 获取测试库
    url(r'getHerders', mamahaoTestCaseAction.getHerders, ),  # 获取头部信息库

    url(r'AssertionCase', mamahaoTestCaseAction.AssertionCase, ),  # 获取头部信息库

    url(r'gettingUseCases', mamahaoTestPlanAction.gettingUseCases, ),  # 获得多级联动筛选条件
    url(r'getTestPlanCases', mamahaoTestPlanAction.getTestPlanCases, ),  # 获得添加测试用例计划列表
    url(r'addTestPlan', mamahaoTestPlanAction.addTestPlan, ),  # 添加测试计划
    url(r'queryTestPlan', mamahaoTestPlanAction.queryTestPlan, ),  # 查询测试计划
    url(r'deleteTestPlan', mamahaoTestPlanAction.deleteTestPlan, ),  # 删除测试计划

    url(r'executetTestPlan', mamahaoTestPlanAction.executetTestPlan, ),  # 删除测试计划

    url(r'addPublicInterface', mamahaoPubInter.addPublicInterface, ),  # 新增公共测试计划
    url(r'queryPublicInterface', mamahaoPubInter.queryPublicInterface, ),  # 查询公共测试计划
    url(r'runNerPublicInterface', mamahaoPubInter.runNerPublicInterface, ),  # 运行公共测试计划
    url(r'delPublicInterface', mamahaoPubInter.delPublicInterface, ),  # 删除公共测试计划

    url(r'getReportPlan', mamahaoReportAction.getReportPlan, ),  # 获取报告条数
    url(r'getReportCase', mamahaoReportAction.getReportCase, ),  # 获取报告信息

    url(r'getEmail', mamahaoTestEmail.getEmail, ),  # 获得邮件
    url(r'addEmail', mamahaoTestEmail.addEmail, ),  # 增加修改邮件
    url(r'testEmail', mamahaoTestEmail.testEmail, ),  # 测试邮件

    url(r'adduserPresonnel', mamahaotestPersonnel.adduserPresonnel, ),  # 新增修改用户接口
    url(r'getPresonnel', mamahaotestPersonnel.getPresonnel, ),  # 获取单条接口
    url(r'getALLPresonnel', mamahaotestPersonnel.getALLPresonnel, ),  # 获得所有用户
    url(r'delPresonnel', mamahaotestPersonnel.delPresonnel, ),  # 获得所有用户





    url(r'uploadtoken', upload.get_token, ),#七牛云

]